using HotChocolatePlayground.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace HotChocolatePlayground.Configurations
{
  public class BuildingPropertyConfiguration : IEntityTypeConfiguration<BuildingProperty>
  {
    public void Configure(EntityTypeBuilder<BuildingProperty> builder)
    {
      builder
        .ToTable("BuildingProperties")
        .HasBaseType(typeof(ImmoveableProperty));
    }
  }
}
