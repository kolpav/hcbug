using HotChocolatePlayground.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace HotChocolatePlayground.Configurations
{
  public class BusinessCaseConfiguration : IEntityTypeConfiguration<BusinessCase>
  {
    public void Configure(EntityTypeBuilder<BusinessCase> builder)
    {
    }
  }
}
