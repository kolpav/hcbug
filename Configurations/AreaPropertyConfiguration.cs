using HotChocolatePlayground.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace HotChocolatePlayground.Configurations
{
  public class AreaPropertyConfiguration : IEntityTypeConfiguration<AreaProperty>
  {
    public void Configure(EntityTypeBuilder<AreaProperty> builder)
    {
      builder
        .ToTable("AreaProperties")
        .HasBaseType(typeof(ImmoveableProperty));
    }
  }
}
