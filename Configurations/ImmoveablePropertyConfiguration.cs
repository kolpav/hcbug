using HotChocolatePlayground.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace HotChocolatePlayground.Configurations
{
  public class ImmoveablePropertyConfiguration : IEntityTypeConfiguration<ImmoveableProperty>
  {
    public void Configure(EntityTypeBuilder<ImmoveableProperty> builder)
    {
    }
  }
}
