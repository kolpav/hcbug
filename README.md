OK Query

```
{
  businessCases {
    items {
      id
      immoveableProperty {
        ... on AreaProperty {
          id
        }
        ... on BuildingProperty {
          id
        }
      }
    }
  }
}
```

NOK Query

```
{
  businessCases {
    items {
      id
      immoveableProperty {
        __typename
      }
    }
  }
}
```
