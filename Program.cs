﻿using System.Linq;
using System.Threading.Tasks;
using HotChocolatePlayground.Data;
using HotChocolatePlayground.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace HotChocolatePlayground
{
  public class Program
  {
    public async static Task Main(string[] args)
    {
      var host = CreateHostBuilder(args).Build();
      using var scope = host.Services.CreateScope();
      var services = scope.ServiceProvider;
      var context = services.GetRequiredService<ApplicationDbContext>();
      context.Database.Migrate();

      if (!context.BusinessCases.Any())
      {
        var buildingBusinessCase = new BusinessCase
        {
          ImmoveableProperty = new BuildingProperty
          {
            HasElevator = true
          }
        };
        var areaBusinessCase = new BusinessCase
        {
          ImmoveableProperty = new AreaProperty
          {
            Area = 13
          }
        };

        context.BusinessCases.Add(buildingBusinessCase);
        context.BusinessCases.Add(areaBusinessCase);

        context.SaveChanges();
      }

      await host.RunAsync();
    }

    public static IHostBuilder CreateHostBuilder(string[] args) =>
      Host.CreateDefaultBuilder(args)
        .ConfigureWebHostDefaults(webBuilder =>
          webBuilder.UseStartup<Startup>()
        );
  }
}
