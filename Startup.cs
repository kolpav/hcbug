using System;
using GraphQL.Server.Ui.Playground;
using HotChocolate.Execution.Options;
using HotChocolatePlayground.Data;
using HotChocolatePlayground.Types;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace HotChocolatePlayground
{
  public class Startup
  {
    public Startup(IConfiguration configuration)
    {
      Configuration = configuration;
    }

    public IConfiguration Configuration { get; }

    // This method gets called by the runtime. Use this method to add services to the container.
    public void ConfigureServices(IServiceCollection services)
    {
      services
        .AddDbContext<ApplicationDbContext>(options =>
          options
            .UseNpgsql("Server=localhost;Database=HotChocolatePlayground;Include Error Detail=true")
            .EnableDetailedErrors()
            .EnableSensitiveDataLogging()
            .LogTo(Console.WriteLine)
        )
        .AddGraphQLServer()
        .AddProjections()
        .AddFiltering()
        .AddSorting()
        .SetPagingOptions(new()
        {
          DefaultPageSize = 10,
          MaxPageSize = 100,
          IncludeTotalCount = true
        })
        .ModifyRequestOptions(x => x.IncludeExceptionDetails = true)
        .AddApolloTracing(TracingPreference.Always)
        .AddQueryType<Query>()
        .AddType<AreaPropertyType>()
        .AddType<BuildingPropertyType>()
        .AddType<BusinessCaseType>();
    }

    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
      app.UseRouting();
      app.UseEndpoints(endpoints => endpoints.MapGraphQL());
      app.UseGraphQLPlayground(new PlaygroundOptions
      {
        SchemaPollingInterval = 10_000,
        RequestCredentials = RequestCredentials.Include,
        EditorTheme = EditorTheme.Dark,
        EditorFontSize = 14,
        EditorReuseHeaders = true
      }, "/graphiql");
      app.UseGraphQLVoyager("/voyager");
    }
  }
}
