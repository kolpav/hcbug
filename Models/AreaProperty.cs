namespace HotChocolatePlayground.Models
{
  public class AreaProperty : ImmoveableProperty
  {
    public int Area { get; set; }
  }
}
