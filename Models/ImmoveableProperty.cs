namespace HotChocolatePlayground.Models
{
  public abstract class ImmoveableProperty
  {
    public int Id { get; set; }

    public int BusinessCaseId { get; set; }
    public BusinessCase BusinessCase { get; set; }
  }
}

