namespace HotChocolatePlayground.Models
{
  public class BusinessCase
  {
    public int Id { get; set; }

    public ImmoveableProperty ImmoveableProperty { get; set; }
  }
}
