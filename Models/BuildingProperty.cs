namespace HotChocolatePlayground.Models
{
  public class BuildingProperty : ImmoveableProperty
  {
    public bool? HasElevator { get; set; }
  }
}
