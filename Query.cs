﻿using System.Linq;
using System.Threading.Tasks;
using HotChocolate;
using HotChocolate.Data;
using HotChocolate.Types;
using HotChocolatePlayground.Data;
using HotChocolatePlayground.Models;

namespace HotChocolatePlayground
{
  public class Query
  {
    [Serial]
    [UseOffsetPaging]
    [UseProjection]
    [UseFiltering]
    [UseSorting]
    public IQueryable<BusinessCase> GetBusinessCases(
      [Service] ApplicationDbContext context)
      => context.BusinessCases;
  }
}
