using HotChocolate.Types;
using HotChocolatePlayground.Models;

namespace HotChocolatePlayground.Types
{
  public class AreaPropertyType : ObjectType<AreaProperty>
  {
    protected override void Configure(IObjectTypeDescriptor<AreaProperty> descriptor)
    {
      descriptor.Implements<ImmoveablePropertyType>();
    }
  }
}
