using HotChocolate.Types;
using HotChocolatePlayground.Models;

namespace HotChocolatePlayground.Types
{
  public class ImmoveablePropertyType : InterfaceType<ImmoveableProperty>
  {
    protected override void Configure(IInterfaceTypeDescriptor<ImmoveableProperty> descriptor)
    {
      descriptor.Name("IImmoveable");
    }
  }
}
