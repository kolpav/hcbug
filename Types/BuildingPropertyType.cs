using HotChocolate.Types;
using HotChocolatePlayground.Models;

namespace HotChocolatePlayground.Types
{
  public class BuildingPropertyType : ObjectType<BuildingProperty>
  {
    protected override void Configure(IObjectTypeDescriptor<BuildingProperty> descriptor)
    {
      descriptor.Implements<ImmoveablePropertyType>();
    }
  }
}
