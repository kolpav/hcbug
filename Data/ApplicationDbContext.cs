using System.Reflection;
using HotChocolatePlayground.Models;
using Microsoft.EntityFrameworkCore;

namespace HotChocolatePlayground.Data
{
  public class ApplicationDbContext : DbContext
  {
    public DbSet<BusinessCase> BusinessCases { get; set; }
    public DbSet<AreaProperty> AreaProperties { get; set; }
    public DbSet<BuildingProperty> BuildingProperties { get; set; }

    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
      : base(options) { }

    protected override void OnModelCreating(ModelBuilder builder)
    {
      base.OnModelCreating(builder);
      builder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
    }
  }
}
